-- WARNING --

When set up correctly with a firewall or similar this module 
will slow down and block traffic to your website. This potentially could 
include yourself, website owners, maintainers and legitimate visitors to your 
website. If you use this module then you do so at your own risk.

-- SUMMARY --

The Honeytrap module allows site owners and system administrators to monitor 
web crawlers that do not follow the rules set out in the robot.txt file 
or via the RobotsText module or similar method and as a result put an 
unnecessarily high load on servers.

This is especially important for large, very high traffic and/or high 
profile sites where the activity of these non-compliant crawlers can bring 
servers to their knees. If these crawlers are not blocked or slowed down 
quickly enough then these crawlers can result in servers being knocked 
completely offline.

Note: The Honeytrap module does not directly block or slow down offending 
IP addresses itself; it only logs and reports them, leaving you in full 
control of how you want to deal with them.

-- REQUIREMENTS --

* Views 2 module
* A configurable firewall (ideally)
* A running Drupal cron task (ideally)
* RobotsTxt module or access to the robots.txt file
* This module will not currently work on a Windows environment because it 
  assumes that the '/' character separates path components.

-- INSTALLATION --

* Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-5-6.

-- MORE INFORMATION --

For much more detailed help and information see the README.html file in the 
docs folder.

-- CREDITS --

* Created by: Mikey Bunny

* Sponsored by:

Moo Free Chocolates
http://www.moofreechocolates.com
Manufacturer of scrummy dairy free chocolates.
