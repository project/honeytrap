<?php
/**
 * @file
 * The Honeytrap admin form function definitions.
 *
 * @ingroup forms
 */

/**
 * Form builder for the settings form.
 *
 * @param $form_state
 *   Standard hook_form() parameter
 *
 * @return The form structure array.
 *
 * @see honeytrap_settings_form_validate()
 * @see honeytrap_settings_form_submit()
 *
 * @ingroup forms
 */
function honeytrap_settings_form($form_state) {
  $trap = honeytrap_get_trap_array();
  if ($trap) {
    drupal_set_message(
      html_entity_decode(
        t(
          'An example trap URL based on your current settings is <em>%url</em>.
Visit this page to get added to the naughty list.',
          array('%url' => l($trap['url'], $trap['url']))
        )
      )
    );
  }

  // Get any existing settings
  $honeytrap_settings = variable_get('honeytrap_settings', array());

  // Construct the form
  $form = array();

  // Honeytrap url settings
  $form['settings']['url'] = array(
    '#type' => 'fieldset',
    '#title' => t('Honeytrap url'),
    '#description' => html_entity_decode(
      t(
        '<b>IMPORTANT:</b>
These paths must not already exist on your site.
Honeytrap will add hidden links to these paths at the bottom of each page.<br />
<br />
The %robotstxt module, if enabled,
will warn well-behaved web-crawlers to ignore these links.
Otherwise, the paths must be added by manually editing your robots.txt file.',
        array(
          '%robotstxt' => l(
           'RobotsTxt',
           'http://drupal.org/project/robotstxt',
           array('attributes' => array('target' => 'robotstxt'))
         )
        )
      )
    ),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $disallowed_paths = implode("\n", $honeytrap_settings['url']['disallowed_paths']);
  $form['settings']['url']['disallowed_paths'] =
  array(
    '#type' => 'textarea',
    '#title' => t('Disallowed paths'),
    '#description' => html_entity_decode(
      t(
        'Use one entry per line.<br />
<br />
<b>Example:</b> If you add a line "example",
then the IP address of anyone who visits the URL "/example" will be added to the naughty list.
If you have the %robotstxt module installed,
then the entries "Disallow: /example" (clean URLs),
and "Disallow: /?q=example" (no clean URLs) will be added to your robots.txt file.
You should manually add the correct entries if you do not have the %robotstxt module enabled.',
        array(
          '%robotstxt' => l(
          'RobotsTxt',
          'http://drupal.org/project/robotstxt',
        array('attributes' => array('target' => 'robotstxt'))
      )
    )
  )

    ),
    '#default_value' => $disallowed_paths,
    '#cols' => 60,
    '#rows' => 5,
    '#required' => TRUE,
  );

  $form['rand_min'] = array('#type' => 'hidden', '#value' => $honeytrap_settings['url']['rand_min']);
  $form['rand_max'] = array('#type' => 'hidden', '#value' => $honeytrap_settings['url']['rand_max']);

  // List file settings
  $form['settings']['list_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('List files (optional)'),
    '#description' => t(
      'These files will be required by some firewalls.
If used, the folders for these files must exist and be writable.
File names will be evaluated from the current working directory.<br />
For example, %relative is in the default Drupal files directory,
but %absolute is in the Unix filesystem temporary directory.',
      array(
        '%relative' => 'sites/default/files/list.txt',
        '%absolute' => '/tmp/list.txt',
      )
    ),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['settings']['list_files']['list_filename_black'] = array(
    '#type' => 'textfield',
    '#title' => t('Black list filename'),
    '#description' => t(
      'Leave blank if your firewall does not need this file.<br />
The IP addresses added by Honeytrap to this file should be blocked by firewalls.'
    ),
    '#default_value' => $honeytrap_settings['list_files']['black'],
    '#size' => 80,
    '#maxlength' => 254,
    '#required' => FALSE,
  );

  $form['settings']['list_files']['list_filename_white'] = array(
    '#type' => 'textfield',
    '#title' => t('White list filename'),
    '#description' => t(
      'Leave blank if your firewall does not need this file.<br />
IP addresses added by Honeytrap to this file should always be allowed by firewalls.'
    ),
    '#default_value' => $honeytrap_settings['list_files']['white'],
    '#size' => 80,
    '#maxlength' => 254,
    '#required' => FALSE,
  );

  $form['settings']['list_files']['list_filename_naughty'] = array(
    '#type' => 'textfield',
    '#title' => t('Naughty list filename'),
    '#description' => t(
      'Leave blank if your firewall does not need this file.<br />
IP addresses added by Honeytrap to this file should nomally be throttled by firewalls.'
    ),
    '#default_value' => $honeytrap_settings['list_files']['naughty'],
    '#size' => 80,
    '#maxlength' => 254,
    '#required' => FALSE,
  );

  // Cron task settings
  $form['settings']['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Honeytrap cron'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Minimum amount of time before an automatically added entry expires
  $form['settings']['cron']['expired_entry_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Auto expiry time in seconds (0 for none)'),
    '#description' => t(
      'The minimum amount of time in seconds that an automatically added entry will remain on the lists.'
    ),
    '#default_value' => $honeytrap_settings['cron']['expired_entry_time'],
    '#size' => 8,
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

/**
 * Form validation handler for honeytrap_settings_form().
 *
 * @param $form
 *   Standard hook_validate() parameter
 * @param $form_state
 *   Standard hook_validate() parameter
 *
 * @see honeytrap_settings_form()
 * @see honeytrap_settings_form_submit()
 *
 * @ingroup forms
 */
function honeytrap_settings_form_validate($form, &$form_state) {
  // Check that the minimum random value is less than or equal to the maximum
  // random value
  if ($form_state['values']['rand_min'] > $form_state['values']['rand_max']) {
    form_set_error(
      'rand_min',
      t('The minimum random number for the url must be less than or equal to the maximum random number.')
    );
  }

  // Check if the black list filename is set and make sure that it can be written
  if (
    !empty($form_state['values']['list_filename_black']) &&
    !is_writable(dirname($form_state['values']['list_filename_black']))
  ) {
    form_set_error(
      'list_filename_black',
      t(
        'The black list filename is not valid.
Please check that the folder exists and is writeable.',
        array('%filename' => $form_state['values']['list_filename_black'])
      )
    );
  }

  // Check if the white list filename is set and make sure that it can be written
  if (
    !empty($form_state['values']['list_filename_white']) &&
    !is_writable(dirname($form_state['values']['list_filename_white']))
  ) {
    form_set_error(
      'list_filename_white',
      t(
        'The white list filename is not valid.
Please check that the folder exists and is writeable.',
        array('%filename' => $form_state['values']['list_filename_white'])
      )
    );
  }

  // Check if the naughty list filename is set and make sure that it can be written
  if (
    !empty($form_state['values']['list_filename_naughty']) &&
    !is_writable(dirname($form_state['values']['list_filename_naughty']))
  ) {
    form_set_error(
      'list_filename_naughty',
      t(
        "The naughty list filename is not valid.
Please check that the folder exists and is writeable.",
        array('%filename' => $form_state['values']['list_filename_naughty'])
      )
    );
  }

  // Get the disallowed paths
  $disallowed_paths = explode("\n", $form_state['values']['disallowed_paths']);
  $corrected_paths = array();
  foreach ($disallowed_paths as $index => $path) {
    // Remove any whitespace and leading/trailing slashes for menu checks
    $path = check_plain(trim($path, " /\r\n"));

    // Check if the path already exists
    $router_item = menu_get_item($path);
    if (($router_item !== FALSE) && ($router_item['page_callback'] != 'honeytrap_naughty_link')) {
      form_set_error(
        'disallowed_paths',
        t(
          'The menu path %path already exists.
Please choose something more obscure.',
          array('%path' => $path)
        )
      );
    }
    else {
      $corrected_paths[] = $path;
    }
  }

  if (count($corrected_paths)) {
    $form_state['values']['disallowed_paths'] = implode("\n", $corrected_paths);
  }
}

/**
 * Form submission handler for honeytrap_settings_form().
 *
 * @param $form
 *   Standard hook_submit() parameter
 * @param $form_state
 *   Standard hook_submit() parameter
 *
 * @see honeytrap_settings_form()
 * @see honeytrap_settings_form_validate()
 *
 * @ingroup forms
 */
function honeytrap_settings_form_submit($form, &$form_state) {
  // Get the disallowed paths and remove any whitespace
  $disallowed_paths = explode("\n", $form_state['values']['disallowed_paths']);
  foreach ($disallowed_paths as $index => $path) {
    $disallowed_paths[$index] = check_plain(trim($path));
  }

  // Get the previous settings
  $old_honeytrap_settings = variable_get('honeytrap_settings', array());

  // Save the settings
  $honeytrap_settings = array_merge(
    $old_honeytrap_settings,
    array(
      'cron' => array(
        'expired_entry_time' => (int)$form_state['values']['expired_entry_time'],
      ),
      'url' => array(
        'disallowed_paths' => $disallowed_paths,
        'rand_min' => (int)$form_state['values']['rand_min'],
        'rand_max' => (int)$form_state['values']['rand_max'],
      ),
      'list_files' => array(
        'black' => $form_state['values']['list_filename_black'],
        'white' => $form_state['values']['list_filename_white'],
        'naughty' => $form_state['values']['list_filename_naughty'],
      ),
    )
  );
  variable_set('honeytrap_settings', $honeytrap_settings);
  drupal_set_message(t('The settings were updated.'));

  // Check if the paths have changed
  if ($old_honeytrap_settings['url']['disallowed_paths'] != $disallowed_paths) {
    // The disallowed paths have changed, so rebuild the menu to update the
    // hooks for the honeytrap
    menu_rebuild();
    drupal_set_message(t('The disallowed paths were updated.'));
  }

  // Check if any of the list file names have changed
  foreach ($honeytrap_settings['list_files'] as $list => $path) {
    if ($old_honeytrap_settings['list_files'][$list] != $honeytrap_settings['list_files'][$list]) {
      // Regenerate the list file
      _honeytrap_list_file_recreate($list);
    }
  }
}

/**
 * @file
 * Form builder for the add IP form.
 *
 * @param $form_state
 *   Standard hook_form() parameter
 *
 * @return The form structure array.
 *
 * @see honeytrap_add_ip_form_validate()
 * @see honeytrap_add_ip_form_submit()
 *
 * @ingroup forms
 */
function honeytrap_add_ip_form($form_state) {
  // Set the return destination
  $destination = 'admin/settings/honeytrap/list-view';

  // Construct the form
  $form = array();

  drupal_set_message(
    t('Your current IP address is <em>%ip</em>.', array('%ip' => ip_address()))
  );

  $form['details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add IP details'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['details']['new-ip'] = array(
    '#type' => 'textfield',
    '#title' => t('IP address'),
    '#default_value' => '',
    '#size' => 20,
    '#maxlength' => 15,
    '#required' => TRUE,
  );

  $form['details']['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment'),
    '#default_value' => '',
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => FALSE,
  );

  $form['details']['list'] = array(
    '#type' => 'select',
    '#title' => t('List'),
    '#default_value' => '',
    '#options' => honeytrap_get_select_list_array(TRUE),
    '#required' => TRUE,
  );

  $form['created'] = array('#type' => 'hidden', '#value' => time());

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  $form['actions']['cancel'] = array(
    '#value' => l(t('Cancel'), $destination),
  );

  $form['#redirect'] = 'admin/settings/honeytrap/list-view';

  return $form;
}

/**
 * Form validation handler for honeytrap_add_ip_form()
 *
 * @param $form
 *   Standard hook_validate() parameter
 * @param $form_state
 *   Standard hook_validate() parameter
 *
 * @see honeytrap_add_ip_form()
 * @see honeytrap_add_ip_form_submit()
 *
 * @ingroup forms
 */
function honeytrap_add_ip_form_validate($form, &$form_state) {
  // Check that the IP address does not exist already
  if (
    isset($form_state['values']['original-ip']) &&
    ($form_state['values']['new-ip'] != $form_state['values']['original-ip'])
  ) {
    $sql = "SELECT ip FROM {honeytrap_list} WHERE ip = '%s'";
    if ($ip = db_result(db_query($sql, $form_state['values']['new-ip']))) {
      form_set_error(
        'new-ip', t('An entry for the IP address <em>%ip</em> already exists.',
        array('%ip' => $ip))
      );
    }
  }

  // Check if the IP address is valid
  if (!_honeytrap_is_ip_valid($form_state['values']['new-ip'])) {
    form_set_error('new-ip', t('You must enter a valid IP address.'));
  }

  if (!$form_state['values']['list']) {
    form_set_error('list', t('You must select a list to add the IP address to.'));
  }
}

/**
 * Form submission handler for honeytrap_add_ip_form().
 *
 * @param $form
 *   Standard hook_submit() parameter
 * @param $form_state
 *   Standard hook_submit() parameter
 *
 * @see honeytrap_add_ip_form()
 * @see honeytrap_add_ip_form_submit()
 * @see _honeytrap_list_file_add_ip()
 *
 * @ingroup forms
 */
function honeytrap_add_ip_form_submit($form, &$form_state) {
  $sql = "INSERT INTO {honeytrap_list} (ip, created, modified, list, auto, comment, agent)" .
    " VALUES('%s', %d, %d, '%s', 0, '%s', '')";
  if (db_query(
    $sql,
    $form_state['values']['new-ip'],
    $form_state['values']['created'],
    $form_state['values']['created'],
    $form_state['values']['list'],
    $form_state['values']['comment']
  )) {
    drupal_set_message(
      t(
        'The IP address <em>%ip</em> was added to the <em>%list</em> list.',
        array('%ip' => $form_state['values']['new-ip'], '%list' => $form_state['values']['list'])
      )
    );
    _honeytrap_list_file_add_ip($form_state['values']['new-ip'], $form_state['values']['list']);
    _honeytrap_log($form_state['values']['new-ip'], $form_state['values']['list'], t('Manually added'));
  }
  else {
    drupal_set_message(
      t(
        'The IP address <em>%ip</em> could not be added to the <em>%list</em> list.',
        array(
          '%ip' => $form_state['values']['new-ip'],
          '%list' => $form_state['values']['list'],
        )
      ),
      'error'
    );
  }
}

/**
 * @file
 * Form builder for the delete ip confirmation form.
 *
 * Constructs a confirmation form to ensure that the user really wants to remove
 * the specified IP address from the Honeytrap.
 *
 * @param $form_state
 *   Standard hook_form() parameter
 * @param $ip
 *   The IP address to remove from the Honeytrap.
 *
 * @return The form structure array.
 *
 * @see honeytrap_delete_ip_confirmation_form_submit()
 *
 * @ingroup forms
 */
function honeytrap_delete_ip_confirmation_form($form_state, $ip) {
  $form = array();
  $form['ip'] = array('#type' => 'hidden', '#value' => $ip);
  $form['#redirect'] = 'admin/settings/honeytrap/list-view';
  return confirm_form($form,
    t('Remove IP address from the honeytrap'),
    $form['#redirect'],
    t(
      'Are you sure that you want to remove the honeytrap entry for %ip?',
      array('%ip' => $ip)
    ),
    t('Yes'),
    t('No')
  );
}

/**
 * Form submission handler for honeytrap_delete_ip_confirmation_form().
 *
 * @param $form
 *   Standard hook_submit() parameter
 * @param $form_state S
 *   Standard hook_submit() parameter
 *
 * @see honeytrap_delete_ip_confirmation_form()
 * @see _honeytrap_list_file_remove_ip()
 *
 * @ingroup forms
 */
function honeytrap_delete_ip_confirmation_form_submit($form, &$form_state) {
  $ip = $form['ip']['#value'];

  // Determine what list the ip entry is currently on
  $sql = "SELECT list FROM {honeytrap_list} WHERE ip = '%s'";
  $list = db_result(db_query($sql, $ip));

  // Attempt to remove the ip entry
  $sql = "DELETE FROM {honeytrap_list} WHERE ip = '%s'";
  if (db_query($sql, $ip)) {
    drupal_set_message(t('The ip address <em>%ip</em> was removed.', array('%ip' => $ip)));
    _honeytrap_list_file_remove_ip($ip, $list);
  }
  else {
    drupal_set_message(
      t('The ip address <em>%ip</em> could not be removed.', array('%ip' => $ip)),
      'error'
    );
  }
}

/**
 * @file
 * Form builder for the edit IP form.
 *
 * @param $form_state
 *   Standard hook_form() parameter
 * @param $ip
 *   The IP address to remove from the Honeytrap.
 *
 * @return The form structure array.
 *
 * @see honeytrap_edit_ip_form_validate()
 * @see honeytrap_edit_ip_form_submit()
 *
 * @ingroup forms
 */
function honeytrap_edit_ip_form($form_state, $ip) {
  // Load the existing IP details
  $sql = "SELECT * FROM {honeytrap_list} WHERE ip = '%s'";
  $current = db_fetch_array(db_query($sql, $ip));

  // Set the return destination
  $destination = 'admin/settings/honeytrap/list-view';

  // Construct the form
  $form = array();

  $form['details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Editable details'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['details']['new-ip'] = array(
    '#type' => 'textfield',
    '#title' => t('IP address'),
    '#default_value' => $ip,
    '#size' => 20,
    '#maxlength' => 15,
    '#required' => TRUE,
  );

  $form['details']['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment'),
    '#default_value' => $current['comment'],
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => FALSE,
  );

  $form['details']['new-list'] = array(
    '#type' => 'select',
    '#title' => t('List'),
    '#default_value' => $current['list'],
    '#options' => honeytrap_get_select_list_array(),
    '#required' => TRUE,
  );

  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('More information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['other']['agent'] = array(
    '#type' => 'item',
    '#title' => t('User agent details'),
    '#value' => $current['agent'],
  );

  $form['other']['created'] = array(
    '#type' => 'item',
    '#title' => t('Created'),
    '#value' => date('Y-m-d Hi.s', $current['created']),
  );

  $form['other']['modified'] = array(
    '#type' => 'item',
    '#title' => t('Last modified'),
    '#value' => date('Y-m-d Hi.s', $current['modified']),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  $form['actions']['cancel'] = array(
    '#value' => l(t('Cancel'), $destination),
  );

  $form['original-ip'] = array('#type' => 'hidden', '#value' => $ip);
  $form['original-list'] = array('#type' => 'hidden', '#value' => $current['list']);

  $form['#redirect'] = $destination;

  return $form;
}

/**
 * Form validation handler for honeytrap_edit_ip_form_validate_form().
 *
 * @param $form standard
 *   hook_validate() parameter
 * @param $form_state
 *   Standard hook_validate() parameter
 *
 * @see honeytrap_edit_ip_form()
 * @see honeytrap_edit_ip_form_submit()
 * @see _honeytrap_is_ip_valid()
 *
 * @ingroup forms
 */
function honeytrap_edit_ip_form_validate($form, &$form_state) {
  // If the IP address has been changed check that the new IP address does not
  // already exit
  if (
    isset($form_state['values']['original-ip']) &&
    ($form_state['values']['new-ip'] != $form_state['values']['original-ip'])
  ) {
    $sql = "SELECT ip FROM {honeytrap_list} WHERE ip = '%s'";
    if (db_result(db_query($sql, $form_state['values']['new-ip']))) {
      form_set_error('new-ip', t('An IP address with the new value already exists.'));
    }
  }

  // Check if the ip address is valid
  if (!_honeytrap_is_ip_valid($form_state['values']['new-ip'])) {
    form_set_error('new-ip', t('You must enter a valid IP address.'));
  }
}

/**
 * Implementation of hook_submit() for edit_ip_form.
 *
 * @param $form standard
 *   hook_submit() parameter
 * @param $form_state standard
 *   hook_submit() parameter
 *
 * @return The form structure array.
 *
 * @see honeytrap_edit_ip_form_submit()
 * @see honeytrap_edit_ip_form_validate()
 * @see _honeytrap_list_file_update()
 *
 * @ingroup forms
 */
function honeytrap_edit_ip_form_submit($form, &$form_state) {
  // Save the changes to the db
  $sql = "UPDATE {honeytrap_list} SET ip='%s', list='%s', comment='%s', modified=%d, auto=0 where ip='%s'";
  if (db_query(
    $sql,
    $form_state['values']['new-ip'],
    $form_state['values']['new-list'],
    $form_state['values']['comment'],
    time(),
    $form_state['values']['original-ip'])
  ) {
    drupal_set_message(
      t('The changes to <em>%ip</em> were saved.', array('%ip' => $form_state['values']['new-ip']))
    );

    // Check if the IP address or list was changed
    if (
      ($form_state['values']['original-ip'] != $form_state['values']['new-ip']) ||
      ($form_state['values']['original-list'] != $form_state['values']['new-list']) ) {
      // Update the list files
      _honeytrap_list_file_update(
        $form_state['values']['original-ip'], $form_state['values']['original-list'],
        $form_state['values']['new-ip'], $form_state['values']['new-list']
      );
    }
  }
  else {
    drupal_set_message(
      t('The changes to <em>%ip</em> were not saved.',
      array('%ip' => $form_state['values']['new-ip'])), 'error'
    );
  }
}

/**
 * @file
 * Form builder for the maintainance form.
 *
 * @param $form_state
 *   Standard hook_form() parameter
 *
 * @return The form structure array.
 *
 * @see honeytrap_maintenance_form()
 * @see _honeytrap_purge_lists()
 *
 * @ingroup forms
 */
function honeytrap_maintenance_form(&$node) {
  // Get any existing settings
  $honeytrap_settings = variable_get('honeytrap_settings', array());

  // Check if any lists are enables
  $enabled = FALSE;
  foreach ($honeytrap_settings['list_files'] as $list_filename) {
    if (!empty($list_filename)) {
      // At least one list file is in use
      $enabled = TRUE;
      break;
    }
  }

  // Construct the form
  $form = array();

  if ($enabled) {
    $description = t('Remove any old entries from the list files immediately rather than waiting for the cron.');
  }
  else {
    $description = t('You must specify at least one list filename on the <em>Settings</em> page first.');
  }

  $form['lists'] = array(
    '#type' => 'fieldset',
    '#title' => t('List files'),
    '#description' => $description,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['lists']['purge_lists'] = array(
    '#type' => 'submit',
    '#value' => t('Purge list files'),
    '#access' => $enabled,
    '#submit' => array('honeytrap_maintenance_form_submit_purge'),
  );

  return $form;
}

/**
 * Purge button form submission handler for honeytrap_maintenance_form().
 *
 * @param $form
 *   Standard hook_submit() parameter
 * @param $form_state
 *   Standard hook_submit() parameter
 *
 * @see honeytrap_maintenance_form_submit_form()
 * @see _honeytrap_purge_lists()
 *
 * @ingroup forms
 */
function honeytrap_maintenance_form_submit_purge($form, &$form_state) {
  _honeytrap_purge_lists(TRUE);
  drupal_set_message(t('Any expired entries were removed from the list files.'));
}
