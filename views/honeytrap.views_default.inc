<?php
/**
 * @file
 * Definitions for the default Honeytrap views.
 */

/**
 * The default view definition for the honeytrap lists.
 *
 * @see honeytrap_handler_filter_many_to_one()
 * @see honeytrap_views_api()
 * @see honeytrap_views_handlers()
 * @see honeytrap_views_data()
 */
function honeytrap_views_default_views() {
  $view = new view;
  $view->name = 'honeytrap_list';
  $view->description = 'This view is used by the Honeytrap module to surface values that are stored in its lists.';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'honeytrap_list';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE;
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'created' => array(
      'label' => 'created',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'honeytrap_list',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'ip' => array(
      'label' => 'IP Address',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'ip',
      'table' => 'honeytrap_list',
      'field' => 'ip',
      'relationship' => 'none',
    ),
    'list' => array(
      'label' => 'List',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'list',
      'table' => 'honeytrap_list',
      'field' => 'list',
      'relationship' => 'none',
    ),
    'auto' => array(
      'label' => 'Auto generated',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'type' => 'yes-no',
      'not' => 0,
      'exclude' => 0,
      'id' => 'auto',
      'table' => 'honeytrap_list',
      'field' => 'auto',
      'relationship' => 'none',
    ),
    'agent' => array(
      'label' => 'Agent',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 1,
        'max_length' => '30',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'agent',
      'table' => 'honeytrap_list',
      'field' => 'agent',
      'relationship' => 'none',
    ),
    'comment' => array(
      'label' => 'Comment',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 1,
        'max_length' => '30',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 1,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'comment',
      'table' => 'honeytrap_list',
      'field' => 'comment',
      'relationship' => 'none',
    ),
    'modified' => array(
      'label' => 'Modified',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'modified',
      'table' => 'honeytrap_list',
      'field' => 'modified',
      'relationship' => 'none',
    ),
    'options' => array(
      'label' => 'Options',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<span class="honeytrap-options"><a href="/admin/settings/honeytrap/edit/[ip]">edit</a> <a href="/admin/settings/honeytrap/delete/[ip]">delete</a></span>',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'value' => '',
      'format' => '1',
      'exclude' => 0,
      'id' => 'options',
      'table' => 'honeytrap_list',
      'field' => 'options',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'list' => array(
      'operator' => 'or',
      'value' => array(
        'black' => 'black',
        'white' => 'white',
        'naughty' => 'naughty',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'list_op',
        'identifier' => 'list',
        'label' => 'Show only the selected lists?',
        'optional' => 1,
        'single' => 0,
        'remember' => 1,
        'reduce' => 0,
      ),
      'id' => 'list',
      'table' => 'honeytrap_list',
      'field' => 'list',
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
    'auto' => array(
      'operator' => '=',
      'value' => 'All',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'operator' => '',
        'identifier' => 'auto',
        'label' => 'Show only auto generated entries?',
        'optional' => 1,
        'remember' => 1,
      ),
      'id' => 'auto',
      'table' => 'honeytrap_list',
      'field' => 'auto',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'edit honeytraps',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('items_per_page', 25);
  $handler->override_option('use_pager', 'mini');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'created' => 'created',
      'ip' => 'ip',
      'list' => 'list',
      'auto' => 'auto',
      'agent' => 'agent',
      'comment' => 'comment',
      'modified' => 'modified',
      'markup' => 'markup',
    ),
    'info' => array(
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'ip' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'list' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'auto' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'agent' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'comment' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'modified' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'markup' => array(
        'separator' => '',
      ),
    ),
    'default' => 'created',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/settings/honeytrap/list-view');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'View lists',
    'description' => '',
    'weight' => '-7',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;
  return $views;
}