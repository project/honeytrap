<?php
/**
 * @file
 * Class definition for the filter which handles the honeytrap's view select
 * list.
 *
 * @see honeytrap_views_handlers()
 * @see honeytrap_views_default_views()
 * @see honeytrap_views_default_views()
 */
class honeytrap_handler_filter_many_to_one extends views_handler_filter_many_to_one {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = honeytrap_get_select_list_array();
  }
}